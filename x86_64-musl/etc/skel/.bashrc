# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
if [[ ${EUID} == 0 ]] ; then
    PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
else
    PS1='\[\033[01;32m\][\u@\h\[\033[01;37m\] \W\[\033[01;32m\]]\$\[\033[00m\] '
fi

# Disable beep
xset b off

alias disk="lsblk -o NAME,TYPE,FSTYPE,FSAVAIL,FSUSE%,SIZE,LABEL,MOUNTPOINT"
alias ffmpeg="ffmpeg -hide_banner"
alias pp="ping -c3 voidlinux.org"
alias fontfresh="fc-cache -fv"
alias clone="git clone --depth 1"
alias mkd="mkdir -pv"
alias cp="cp -iv"
alias mv="mv -iv"
alias rm="rm -vI"
alias e='exit 0'
alias c='clear'

alias please="sudo"                             # better asking
alias sync="sudo xbps-install -S"               # syncron the repository
alias update="sudo l7-tools --update"           # update packages
alias get="sudo xbps-install"                   # install packages
alias forceget="sudo xbps-install -Sf"          # force install packages
alias info="xbps-query -S"                      # information of packages
alias remove="sudo xbps-remove -R"              # remove some packages
alias forceremove="sudo xbps-remove -R -f"      # force remove some packages
alias autoclean="sudo xbps-remove -v -Oo"       # remove orphaned packages
alias search="xbps-query -v -Rs"                # search packages
alias deps="xbps-query -vRx"                    # depedency of packages
alias installed="xbps-query -v -l"              # list installed
alias filelist="xbps-query --regex -Rf"         # list of the packages
alias repolist="xbps-query -v -L"               # repo list
alias reconfigure="sudo xbps-reconfigure -v"    # reconfigure packages
